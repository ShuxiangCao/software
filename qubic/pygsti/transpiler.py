import pygsti

pygsti_to_qubic = {
        'Gxpi2': 'X90',
        'Gypi2': 'Y90',
        'Gcr': 'CR',
        'Gcz': 'CZ',
        'Gcphase': 'CZ',
        'Gzpi2': 'Z90',
        'Gzr': 'virtual_z',
        'Gxx': ['X90', 'X90'],
        'Gxy': ['X90', 'Y90'],
        'Gyx': ['Y90', 'X90']}


def _parse_layer(layertup, qubit_map):
    layercirc = []
    if layertup.name == 'COMPOUND':
        for layer in layertup:
            layercirc.extend(_parse_layer(layer, qubit_map))
    else:
        if isinstance(pygsti_to_qubic[layertup.name], str):
            layercirc = [{'name': pygsti_to_qubic[layertup.name],
                          'qubit': [qubit_map[n] for n in layertup.qubits]}]
            if layertup.name == 'Gzr':
                layercirc[0]['phase'] = layertup.args[0]
        else:
            layercirc = []
            for i, gatename in enumerate(pygsti_to_qubic[layertup.name]):
                layercirc.append({'name': gatename,
                                  'qubit': [qubit_map[n] for n in layertup.qubits]})
    return layercirc

def transpile(pygsti_circuit, qubit_map, delay_before_circuit=500.e-6):
    qubic_circuit = list()
    qubic_circuit.append({'name': 'delay', 't': delay_before_circuit})
    for layer in pygsti_circuit:
        qubic_circuit.extend(_parse_layer(layer, qubit_map))
        qubic_circuit.append({'name': 'barrier'})
    for qid in pygsti_circuit.line_labels:
        qubic_circuit.append({'name': 'read', 'qubit': qubit_map[qid]})
    return qubic_circuit

# class PyGSTiCircuitMaker:
#     def __init__(self, pspec: QubitProcessorSpec):
#         self.spec = pspec
#         self.target_model = pygsti.models.modelconstruction.create_explicit_model(pspec)
#
#     def update(self, keys_or_dict, value=None):
#         self.qchip.update(keys_or_dict, value)

def shots_to_pygsti_dataset(shot_dictionary, pspec):
    return -1

