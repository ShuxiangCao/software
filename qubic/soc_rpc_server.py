import xmlrpc.server
from qubic.run import CircuitRunner
import logging
import argparse
from functools import partial, update_wrapper
from attrs import define, field
import yaml
from typing import List, Dict, Tuple

@define
class ServerConfig:
    xsa_commit: str
    ip: str
    port: int
    log_level: str
    enable_batching: bool = False
    dac_current_values: Dict = field(default=None)
    dac_nyquist_zone: int = 2
    adc_nyquist_zone: int = 1
    lmk_freq: float = 500.18

    """
    Class for configuring/initializing ZCU216 RPC server. Wraps
    server_config.yaml

    Attributes
    ----------
    xsa_commit: str
        commit hash of gateware build (directory w/ XSA/configs)
    ip: str
        server IP
    port: int
        server port
    log_level: str
    enable_batching: bool 
        if True, allow clients to submit batched jobs, otherwise
        an intermediate job server (job_rpc_server.py) is 
        required.
    dac_current_values: Dict[Tuple, int]
        dictionary of DAC channels to modify current. Format 
        is (block, tile): <current in uA>
    dac_nyquist_zone: int
    adc_nyquist_zone: int
    lmk_freq: float
    """

    def __attrs_post_init__(self):
        if self.dac_current_values is not None:
            self.dac_current_values = {
                    tuple(int(i) for i in key.split(',')): value for key, value in self.dac_current_values.items()}


def run_soc_server(server_config: ServerConfig):
    """
    Start an xmlrpc server that exposes an instance of CircuitRunner over a 
    network. Intended to be run from the RFSoC ARM core python (pynq) 
    environment. IP should only be accessible locally!

    Parameters
    ----------
    server_config: ServerConfig
    """
    runner = CircuitRunner(commit=server_config.xsa_commit,
                           dac_current_values=server_config.dac_current_values,
                           dac_nyquist_zone=server_config.dac_nyquist_zone,
                           adc_nyquist_zone=server_config.adc_nyquist_zone,
                           lmk_freq=server_config.lmk_freq,
                           load_xsa=True)

    server = xmlrpc.server.SimpleXMLRPCServer((server_config.ip, server_config.port), 
                                              logRequests=True, allow_none=True)

    server.register_function(runner.load_circuit)
    server.register_function(runner.run_circuit)
    server.register_function(runner.load_and_run_acq)

    if server_config.enable_batching:
        run_circuit_batch = partial(runner.run_circuit_batch, from_server=True)
        update_wrapper(run_circuit_batch, runner.run_circuit_batch)
        server.register_function(run_circuit_batch)

    print('RPC server running on {}:{}'.format(server_config.ip, server_config.port))

    server.serve_forever()


if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('server_config')
    args = parser.parse_args()

    with open(args.server_config) as f:
        server_config = ServerConfig(**yaml.safe_load(f))
 
    print(f'starting RPC Server on {server_config.ip}:{server_config.port}')

    if server_config.log_level is not None:
        if server_config.log_level.lower() == 'info':
            logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s')

        elif server_config.log_level == 'debug':
            logging.basicConfig(level=logging.DEBUG, format='%(asctime)s %(levelname)s: %(message)s')

        else:
            raise Exception(f'log level: {server_config.log_level} not supported')

    run_soc_server(server_config)


