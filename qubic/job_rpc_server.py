import xmlrpc.server
import logging
import argparse
from typing import List, Dict
import numpy as np

class SoCClient:
    """
    Connect to the SoC via RPC client to run single circuits; expose interface for batched 
    circuit running and single-circuit ADC trace acquisition
    """

    def __init__(self, soc_ip, soc_port):
        self.soc_ip = soc_ip
        self.soc_port = soc_port

    def run_circuit_batch(self, 
                          raw_asm_list: List[Dict], 
                          n_total_shots: int, 
                          reads_per_shot: int | Dict = 1, 
                          timeout_per_shot: float = 8,
                          reload_cmd: bool = True, 
                          reload_freq: bool = True, 
                          reload_env: bool = True, 
                          zero_between_reload: bool = True) -> Dict:
        """
        Runs a batch of circuits given by a list of raw_asm "binaries". Each circuit is run n_total_shots
        times. reads_per_shot, n_total_shots, and delay_per_shot are passed directly into run_circuit, and must
        be the same for all circuits in the batch. The parameters reload_cmd, reload_freq, reload_env, and 
        zero_between_reload control which of these fields is rewritten circuit-to-circuit (everything is 
        rewritten initially). Leave these all at True (default) for maximum safety, to ensure that QubiC 
        is in a clean state before each run. Depending on the circuits, some of these can be turned off 
        to save time.

        TODO: consider throwing some version of all the args here into a BatchedCircuitRun or somesuch
        object

        Parameters
        ----------
            raw_asm_list : list
                list of raw_asm binaries to run
            n_total_shots : int
                number of shots per circuit
            reads_per_shot : int
                number of values per shot per channel to read back from accbuf. Unless
                there is mid-circuit measurement involved this is typically 1
            timeout_per_shot : float
                maximum allowable wall clock time (in seconds) per single shot of the circuit
            reload_cmd : bool
                if True, reload command buffer between circuits
            reload_freq : bool
                if True, reload freq buffer between circuits
            reload_env: bool
                if True, reload env buffer between circuits
        Returns
        -------
            dict:
                Complex IQ shots for each accbuf in chanlist; each array has 
                shape (len(raw_asm_list), n_total_shots, reads_per_shot)
        """
        channels = set().union(*list(set(prog.keys()) for prog in raw_asm_list)) # union of all proc channels in batch
        self.proxy = xmlrpc.client.ServerProxy('http://' + self.soc_ip + ':' + str(self.soc_port), allow_none=True)
        s11 = {ch: bytes() for ch in channels}
        #TODO: using the channels in the first raw_asm_list elem is hacky, should figure out
        # a better way to initialize
        for i, raw_asm in enumerate(raw_asm_list):
            logging.getLogger(__name__).info(f'starting circuit {i}/{len(raw_asm_list)-1}')
            if i==0:
                self.proxy.load_circuit(raw_asm, True, True, True, True)
            else:
                self.proxy.load_circuit(raw_asm, zero_between_reload, reload_cmd, reload_freq, reload_env)

            s11_i = self.proxy.run_circuit(n_total_shots, reads_per_shot, timeout_per_shot, True)
            for ch in s11_i.keys():
                s11[ch] += s11_i[ch].data

        logging.getLogger(__name__).info('batch finished')
        return s11

    def load_and_run_acq(self, raw_asm_prog, n_total_shots=1, nsamples=8192, acq_chans=['0'], 
                        trig_delay=0, decimator=0, return_acc=False, from_server=True):
        self.proxy = xmlrpc.client.ServerProxy('http://' + self.soc_ip + ':' + str(self.soc_port), allow_none=True)
        return self.proxy.load_and_run_acq(raw_asm_prog, n_total_shots, nsamples, acq_chans, trig_delay, decimator, return_acc, True)

def run_job_server(host_ip: str, host_port: int, soc_ip: str, soc_port: int):
    """
    Run the job server.

    Parameters
    ----------
        host_ip: str
        host_port: int
        soc_ip: str
        soc_port: int
    """ 
    soc_client = SoCClient(soc_ip, soc_port)
    job_server = xmlrpc.server.SimpleXMLRPCServer((host_ip, host_port), logRequests=True, allow_none=True)
    job_server.register_function(soc_client.run_circuit_batch)
    job_server.register_function(soc_client.load_and_run_acq)

    print('connected to client {}:{}'.format(soc_ip, soc_port))
    print('RPC job server running on {}:{}'.format(host_ip, host_port))
    job_server.serve_forever()

if __name__=='__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--host-ip', default='192.168.1.25')
    parser.add_argument('--host-port', default=9096)
    parser.add_argument('--soc-ip', default='192.168.1.247')
    parser.add_argument('--soc-port', default=9095)
    parser.add_argument('--log', action='store_true')
    args = parser.parse_args()

    if args.log:
        logging.basicConfig(level=logging.INFO, format='%(asctime)s %(levelname)s: %(message)s')

    run_job_server(args.host_ip, int(args.host_port), args.soc_ip, int(args.soc_port))

