Functions for running an RPC job server on a workstation that is on the same LAN as the ZCU216. Serves as an intermediary between the client (user) machine and the QubiC board. See the [Getting Started Guide](https://gitlab.com/LBL-QubiC/software/-/wikis/Getting-Started-with-QubiC-2.0-on-the-ZCU216) for details on how to configure.

::: qubic.job_rpc_server
